mod args;
use colored::*;
use primes::{PrimeSet, Sieve};

fn main() {
    let args = args::get_args().get_matches();

    let mut pset = Sieve::new();

    if let Some(ix) = args.get_one::<usize>("nth") {
        if *ix > 0 {
            println!("{}", pset.get(*ix - 1));
        }
    }

    if let Some(check) = args.get_many::<String>("check") {
        let nums: Vec<u64> = check.map(|x| x.parse::<u64>().unwrap()).collect();
        for p in nums {
            if pset.is_prime(p) {
                println!("{} {}", p.to_string().green(), "is a prime".green())
            } else {
                println!("{} {}", p.to_string().red(), "is not a prime".red())
            }
        }
    }

    if let Some(list) = args.get_many::<String>("list") {
        let nums: Vec<u64> = list.map(|x| x.parse::<u64>().unwrap()).collect();
        let mut start = nums[0];
        let n: usize;

        if let Some(s) = nums.get(1) {
            n = *s as usize;
        } else {
            n = start as usize;
            start = 0;
        }
        let (ix, _p) = pset.find(start);
        for (_ix, p) in pset.iter().enumerate().skip(ix).take(n) {
            println!("{}", p);
        }
    }

    if let Some(bw) = args.get_many::<String>("between") {
        let nums: Vec<u64> = bw.map(|x| x.parse::<u64>().unwrap()).collect();
        let start = nums[0];
        let end = nums[1];
        let (st_ix, _p) = pset.find(start);
        let (ed_ix, end_p) = pset.find(end);
        for (_, p) in pset.iter().enumerate().skip(st_ix).take(ed_ix - st_ix) {
            println!("{}", p);
        }

        if end_p == end {
            println!("{}", end_p);
        }
    }

}
