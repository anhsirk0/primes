use clap::{Arg, Command, value_parser};

pub fn get_args() -> Command {
    Command::new("Prime numbers generator / checker")
        .author("Anhsirk0, krishna404@yandex.com")
        .version("2.0.0")
        .arg(
            Arg::new("nth")
                .long("nth")
                .short('n')
                .help("Print nth prime")
                .value_parser(value_parser!(usize)),
        )
        .arg(
            Arg::new("list")
                .long("list")
                .short('l')
                .help("Print n primes starting from num2 [default: 0]")
                .num_args(1..3),
        )
        .arg(
            Arg::new("check")
                .long("check")
                .short('c')
                .help("Check if a number(s) is prime")
                .num_args(1..),
        )
        .arg(
            Arg::new("between")
                .long("between")
                .short('b')
                .help("Print primes between num1 num2")
                .num_args(2),
        )
}
