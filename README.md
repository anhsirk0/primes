# Primes

### Prime numbers generator / checker

```bash
$ primes --check 10 11 12 13
10 is not a prime
11 is a prime
12 is not a prime
13 is a prime
```

```bash
$ primes --nth 10
29
```

```bash
$ primes --between 4 16
5
7
11
13
```

```bash
$ primes --list 6
2
3
5
7
11
13
```

```bash
$ primes --list 4 6   # list 6 starting from 4 
5
7
11
13
17
19
```

## Usage
```text
Usage: primes [OPTIONS]

Options:
  -n, --nth <nth>                    Print nth prime
  -l, --list <list>...               Print n primes starting from num2 [default: 0]
  -c, --check <check>...             Check if a number(s) is prime
  -b, --between <between> <between>  Print primes between num1 num2
  -h, --help                         Print help
  -V, --version                      Print version
```
